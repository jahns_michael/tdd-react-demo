import './App.css';
import Counter from "./components/Counter"

function App() {
  return (
    <div className="App">
      <h1 id="greetings">Selamat Datang</h1>
      <Counter id="counter1" />
      <Counter id="counter2" />
    </div>
  );
}

export default App;
