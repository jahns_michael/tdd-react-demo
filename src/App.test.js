import { shallow } from 'enzyme';
import App from './App';

describe("Task 1: Page has a greetings element with 'Selamat datang' message", () => {

  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<App />);
  })

  it("render properly", () => {
    expect(wrapper).toBeTruthy();
  })

  it("there is a greetings element with 'Selamat Datang' message", () => {
    expect(wrapper.find('#greetings').text()).toBe("Selamat Datang");
  })
})

describe("Task 2: Page has two Counter components", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<App />);
  })

  it('has two counter', () => {
    expect(wrapper.find('#counter1').exists()).toBe(true);
    expect(wrapper.find('#counter2').exists()).toBe(true);
  })
})