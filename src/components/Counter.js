import { useState } from "react";

function Counter() {
    const [text, setText] = useState(0)
    return (
        <button onClick={() => setText(text + 1)} >{text}</button>
    )
}

export default Counter;