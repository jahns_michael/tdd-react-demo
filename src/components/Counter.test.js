import { shallow } from 'enzyme';
import Counter from './Counter';

describe("Task 2a: There is a Counter button component", () => {
  
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<Counter />);
  })

  it("render properly", () => {
    expect(wrapper).toBeTruthy();
  })

  it("has a button", () => {
    expect(wrapper.find('button').exists()).toBe(true);
  })

  it('change text when clicked', () => {
    wrapper.find('button').first().simulate("click");
    expect(wrapper.find('button').first().text()).toBe("1");
  })
})